#!/bin/sh

# Ativa o ambiente virtual
. /opt/venv/bin/activate

# Função para esperar a disponibilidade do PostgreSQL
wait_for_postgres() {
    echo "Waiting for PostgreSQL..."

    while ! psql -h $SQL_HOST -U $SQL_USER -d $SQL_DATABASE -c '\q' 2>/dev/null; do
        sleep 0.1
    done

    echo "PostgreSQL is available"
}

# Espera a disponibilidade do PostgreSQL se o tipo de banco de dados for "postgres"
if [ "$DATABASE" = "postgresql" ]; then
    wait_for_postgres
fi

# Define a senha do superusuário do PostgreSQL usando a variável SQL_PASSWORD
export POSTGRES_PASSWORD="$SQL_PASSWORD"

# python manage.py flush --no-input
# python manage.py migrate

# Executa o comando passado como argumento
exec "$@"