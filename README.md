# Django Boilerplate

## Configuração

No diretório raiz do projeto vamos criar o arquivo `.env.dev` com o seguinte conteúdo:

```
DEBUG=1
SECRET_KEY=foo
DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 [::1]
SQL_ENGINE=django.db.backends.postgresql
SQL_DATABASE=django_dev
SQL_USER=django
SQL_PASSWORD=django
SQL_HOST=db
SQL_PORT=5432
DATABASE=postgres
POSTGRES_USER=django
POSTGRES_PASSWORD=django
POSTGRES_DB=django_dev
```

Para construir a aplicação executamos o seguinte comando:

```
docker-compose build
```

Então podemos executar o projeto:

```
docker-compose up
```

Conectando ao banco de dados:

```
docker-compose exec db bash
psql --username=django --dbname=django_dev
```

Acessando o shell da aplicação web:

```
docker-compose exec web bash
docker exec -it app_web_1 sh
/opt/venv/bin/python
```
